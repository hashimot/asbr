<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>Compre Já</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    @yield("css")
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    @yield("javascript-top")
</head>
<body>
<div class="container">
    <div class="row" style="margin:30px 0">
        <div class="col-lg-3">
            <img src="{{ URL::asset('imgs/logo.png') }}" class="img-thumbnail">
        </div>
        <div class="col-lg-9">
            <h3>Nome do Produto</h3>
        </div>
    </div>
    @yield("content")
</div>
@yield("javascript-bottom")
</body>
</html>