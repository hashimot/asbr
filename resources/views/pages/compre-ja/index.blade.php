@extends('layouts.master')
@section('css')
    <style>
        .loading {
            background:url('') no-repeat right center;
        }
    </style>
@stop
@section("content")
    <div class="row">
        <div class="col-lg-6" id="form-container">
            <div id="last-step-loading" class="text-center" style="display: none;">
                <i class="fa fa-cog fa-spin fa-2x fa-fw"></i>
                <span>Carregando...</span>
            </div>
            <div class="errors alert alert-danger" style="display: none;"></div>
            <form id="step_1" class="form-step">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Preencha seus dados para receber contato
                        </div>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="row form-group ">
                                <div class="col-lg-6">
                                    <label>Nome Completo</label>
                                    <input class="form-control" type="text" name="nome" value="" required="required">
                                </div>
                                <div class="col-lg-6">
                                    <label>Data de Nascimento</label>
                                    <input class="form-control" type="text" name="data_nascimento" id="data_nascimento">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-lg-6">
                                    <label>Email</label>
                                    <input class="form-control" type="text" name="email">
                                </div>
                                <div class="col-lg-6">
                                    <label>Telefone</label>
                                    <input class="form-control" type="text" name="telefone" id="telefone">
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-lg btn-info next-step">Próximo Passo</button>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </form>
            <form id="step_2" class="form-step" style="display: none;">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Preencha seus dados para receber contato
                        </div>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="row form-group">
                                <div class="col-lg-6">
                                    <label>Região</label>
                                    <select class="form-control" name="regiao" id="regiao">
                                        <option value="">Selecione a sua região</option>
                                        @if(isset($regioes))
                                            @foreach($regioes AS $reg)
                                                <option value="{{ $reg->id }}">{{ $reg->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <div id="unidade-loading" style="display: none;">
                                        <i class="fa fa-cog fa-spin fa-1x fa-fw"></i>
                                        <span>Carregando...</span>
                                    </div>
                                    <div class="unidade" style="display: none;">
                                        <label>Unidade</label>
                                        <select class="form-control" name="unidade" id="unidade"></select>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-lg btn-info next-step">Enviar</button>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </form>
            <div id="step_sucesso" class="last-step" style="display:none">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Obrigado pelo cadastro!
                        </div>
                    </div>
                    <div class="panel-body">
                        Em breve você receberá uma ligação com mais informações!
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <h1>Chamada interessante para o produto</h1>
            <h2>Mais uma informação relevante</h2>
        </div>
    </div>
@stop
@section("javascript-bottom")
    <script>
        $(document).ready(function(){
            var $errors = $(".errors");
            function displayErrors(type, msg){
                if( msg !== ""){
                    if(type === "text"){
                        $errors.fadeIn('slow').text(msg);
                    } else {
                        $errors.fadeIn('slow').html(msg);
                    }
                }
            }
            $('.next-step').on("click", function (event) {
                var allAnswered = true;
                var hasError = false;
                var closestForm = $(this).closest('form');
                $errors.hide();
                closestForm.find(":input").each(function(){
                    var p = $(this);
                    var typeElement = p.attr('type');
                    if(p.val() === "" && typeElement !== "submit" && typeElement !== "button"){
                        allAnswered = false;
                    }
                });
                if(allAnswered){
                    closestForm.find(".form-group").each(function(){
                        if( $(this).hasClass("has-error") ){ hasError = true; }
                    });
                    if(hasError){
                        displayErrors("text", "Por favor, corrija os erros.");
                    } else {
                        $(this).parents('.form-step').hide().next('.form-step').show();
                    }
                    return false;

                } else {
                    displayErrors("text","Por favor, preenche seus dados.");
                }
            });

            $("#regiao").on("change", function(){
                var e = $(this);
                var opt_unidade = '<option value="">Selecione a unidade mais próxima</option>';
                $("#unidade-loading").show();
                $(".unidade").hide();
                if(e.val() !== ""){
                    $.ajax({
                        url: "{{ URL::action('LeadController@getUnit') }}",
                        type: "POST",
                        data: {
                            _token: $('meta[name=_token]').attr('content'),
                            regiao: $('#regiao').val()
                        },
                        beforeSend: function() {
                            $('#unidade-loading').show();
                        },
                        success: function(result){
                            var json = JSON.parse(result);
                            $.each(json.unit, function(iUnit, thisUnit){
                                var idUnit = thisUnit.id;
                                var nameUnit = thisUnit.name;
                                opt_unidade += '<option value="' + idUnit + '">' + nameUnit +'</option>';
                            });
                            $("#unidade").html(opt_unidade);
                        },
                        complete: function(){
                            $('#unidade-loading').hide();
                            $(".unidade").show();
                        }
                    });
                } else {
                    $("#unidade").html(opt_unidade);
                    $("#unidade-loading").hide();
                    $(".unidade").hide();
                }

            });
            $(".next-step:last").on("click", function(event){
                event.preventDefault();
                $(".form-step").hide();
                $("#last-step-loading").show();
                $errors.hide();
                var formData = $("#step_1, #step_2").serializeArray();
                $.ajax({
                    url: "{{ URL::action('LeadController@submitForm') }}",
                    type: "GET",
                    data: formData,
                    beforeSend: function() {
                        $('#last-step-loading').show();
                    },
                    success: function(result){
                        //A API da ASBR retorna 'success' caso os dados fossem enviados com sucesso.
                        if(result.success){
                            $errors.hide();
                            $(".last-step").fadeIn();
                            alert(result.message);
                        } else {
                            $(".form-step").eq(0).show();
                            $(".last-step").hide();
                            //Na API da ASBR apresenta o campo que ocorreu o erro.
                            displayErrors("text", result.message);
                        }
                    },
                    complete: function(){
                        $('#last-step-loading').hide();
                    },
                    error: function(data){
                        var html_errors = "";
                        var errors = data.responseJSON;
                        var hasErrors = false;
                        var columnNames = [];
                        $.each(errors, function(name, error_value){
                            html_errors += error_value+"<br/>";
                            hasErrors = true;
                            $("[name=" + name + "]").closest('.form-group').addClass('has-error'); // add the Bootstrap error class to the control group
                            columnNames.push(name);
                        });
                        if(hasErrors === true && columnNames.length > 0){
                            var firstName = columnNames[0];
                            $("[name=" + firstName + "]").closest("form").show();
                            displayErrors("html", html_errors);
                        }
                    }
                });
            });
            var maskBehavior = function (val){
                        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
                    },
                    options = {onKeyPress: function(val, e, field, options) {
                        field.mask(maskBehavior.apply({}, arguments), options);
                    }};
            $('#telefone').mask(maskBehavior, options);
            $('#data_nascimento').mask("99/99/9999");
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.min.js"></script>
    <script src="{{ URL::asset('js/jsvalidation.js') }}"></script>
    <script>
        jQuery(document).ready(function(){

            $("#step_1, #step_2").validate({
                errorElement: 'span',
                errorClass: 'help-block error-help-block',

                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length ||
                            element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                        error.insertAfter(element.parent());
                        // else just place the validation message immediatly after the input
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error'); // add the Bootstrap error class to the control group
                },


                /*
                 // Uncomment this to mark as validated non required fields
                 unhighlight: function(element) {
                 $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                 },
                 */
                success: function(element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // remove the Boostrap error class from the control group
                },

                focusInvalid: false, // do not focus the last invalid input

                rules: {"nome":{"laravelValidation":[["Required",[],"Campo Nome vazio. ",true],["Min",["2"],"O nome deve ter pelo menos 2 caracteres. ",false]]},"data_nascimento":{"laravelValidation":[["Required",[],"Campo Data Nascimento vazio.",true],["DateFormat",["d\/m\/Y"],"A data de nascimento n\u00e3o est\u00e1 no formato dd\/mm\/aaaa.",false]]},"email":{"laravelValidation":[["Required",[],"Campo E-mail vazio.",true],["Email",[],"O email deve ser um e-mail v\u00e1lido.",false]]},"telefone":{"laravelValidation":[["Required",[],"Campo Telefone vazio.",true],["Min",["14"],"O telefone deve ter pelo menos 14 caracteres.",false]]},"regiao":{"laravelValidation":[["Required",[],"Campo Regi\u00e3o vazio.",true]]},"unidade":{"laravelValidation":[["Required",[],"Campo Unidade vazio.",true]]}}        })
        })
    </script>
@stop