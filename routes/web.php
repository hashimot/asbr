<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ['as' => 'lead-index', 'uses' => 'LeadController@index']);
Route::get('/asbr', ['as' => 'lead-index', 'uses' => 'LeadController@index']);

Route::post('/asbr/getUnit', ['as' => 'lead-getUnit', 'uses' => 'LeadController@getUnit']);
Route::get('/asbr/submitForm', ['as' => 'lead-submit-form', 'uses' => 'LeadController@submitForm']);
