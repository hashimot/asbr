<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeadRequest;
use App\Lead;
use Illuminate\Http\Request;
use App\Regiao; //Model
use App\Unit;
use Psy\Util\Json;




class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $r = new Regiao;
        $regioes = $r->getAllRegions();
        return view('pages.compre-ja.index')->with(compact('regioes'));
    }

    public function getUnit(Request $request){
        if($request->ajax()){
            $regiao = $request->input('regiao');
            $regiao = Regiao::where('id', '=', $regiao)->with('Unit')->firstOrFail();
            return Json::encode($regiao);
        }
    }

    public function submitForm(LeadRequest $request){
    //public function submitForm(Request $request){
        $lead = new Lead($request);
        $resp = $lead->sendApiLead();
        if($resp->success){
            $lead->insertLead();
            //var_dump("asd");
            return response()->json($resp);
        } else {
            return response()->json($resp);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
