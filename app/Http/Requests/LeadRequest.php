<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|min:2',
            'data_nascimento' => 'required|date_format:d/m/Y',
            'email' => 'required|email',
            'telefone' => 'required|min:14',
            'regiao' => 'required',
            'unidade' => 'required'
        ];
    }

    public function messages(){
        return [
          'nome.required' => "Campo Nome vazio. ",
            'nome.min' => "O nome deve ter pelo menos 2 caracteres. ",
            'data_nascimento.required' => 'Campo Data Nascimento vazio.',
            'data_nascimento.date' => 'A data de nascimento  não é uma data válida.',
            'data_nascimento.date_format' => 'A data de nascimento não está no formato dd/mm/aaaa.',
            'email.required' => 'Campo E-mail vazio.',
            'email.email' => 'O email deve ser um e-mail válido.',
            'telefone.required' => 'Campo Telefone vazio.',
            'telefone.min' => 'O telefone deve ter pelo menos 14 caracteres.',
            'regiao.required' => 'Campo Região vazio.',
            'unidade.required' => 'Campo Unidade vazio.'
        ];
    }
}
