<?php
namespace App\Lib\Calculos;
use App\Regiao;

class ScoreLead
{
    private $unidade;
    private $idade;
    private $score;

    public function __construct($unidade,  $idade, $score = 10)
    {
        $this->unidade = $unidade;
        $this->idade = $idade;
        $this->score = $score;
    }

    public function scorePerRegion()
    {
        if($this->unidade->name === "São Paulo"){
            $this->score += 0;
        } else {
            $this->score += ($this->unidade->region->pontos);
        }
    }

    public function scorePerAge()
    {
        $idade = $this->idade;
        if ($idade >= 40 && $idade <= 99) {
            $this->score += (-3);
        } else if ($idade >= 18 && $idade < 40) {
            $this->score += (0);
        } else if ($idade >= 100) {
            $this->score += (-5);
        } else if ($idade <= 18){
            $this->score += (-5);
        }
    }

    public function getScore(){
        return $this->score;
    }
}