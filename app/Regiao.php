<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regiao extends Model
{
    protected $table = 'regioes';
    protected $primaryKey = "id";


    //Uma região pode ter muitas unidades
    public function Unit(){
        return $this->hasMany('App\Unidade','regiao_id');
    }

    public function getAllRegions(){
        return $this->all();
    }

    public function getPointsByRegion($id, $unidade){
        $region = $this->where('id', $id )->with('Unit')->first();
        if($region['name'] === "Sudeste"  && $unidade === "São Paulo"){
            return 0;
        } else {
            return $region['pontos'];
        }
    }
}
