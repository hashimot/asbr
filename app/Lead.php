<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use App\Lib\Calculos\ScoreLead;
use Carbon\Carbon;
use App\Unidade;

class Lead extends Model
{
    protected $table = "leads";
    protected $fillable = ['nome', 'telefone', 'idade', 'email', 'telefone', 'data_nascimento', 'created_at', 'updated_at'];

    public function __construct($r){
        $this->nome = $r->input('nome');
        $this->unidade_id = $r->input('unidade');
        $this->email = $r->input('email');
        $this->telefone = $r->input('telefone');
        $this->data_nascimento = $this->date_y_m_d_format($r->input('data_nascimento'));
        $s = new ScoreLead($this->unidade, $this->age());
        $s->scorePerRegion();
        $s->scorePerAge();
        $this->score = $s->getScore();
    }

    private function date_y_m_d_format($dt){
        return Carbon::createFromFormat('d/m/Y', $dt)->format('Y-m-d');
    }

    private function age(){
        $new_dt_nasc = Carbon::parse($this->data_nascimento);
        $dt_now = Carbon::createFromDate(2016, 06, 1);
        return  $dt_now->diffInYears($new_dt_nasc);
    }

    public function insertLead(){
        return $this->save();
    }
    public function unidade(){
        return $this->belongsTo('App\Unidade');
    }
    public function sendApiLead(){
        $client = new Client(['http_errors' => false]);

        $res = $client->request('POST',
            'http://api.actualsales.com.br/join-asbr/ti/lead', // url da API
            [
                'form_params' => [
                    'nome' => $this->nome,
                    'email' => $this->email,
                    'telefone' => $this->telefone,
                    'regiao' => $this->unidade->region->name,
                    'unidade' => $this->unidade->name,
                    'data_nascimento' => $this->data_nascimento,
                    'score' => $this->score,
                    'token' => 'c9998cf3e49292ef625082943bf5d203'
                ]
            ]);
        return json_decode($res->getBody());
    }


}
