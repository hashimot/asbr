<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidade extends Model
{
    //
    protected $table = "unidades";
    protected $primaryKey = "id";

    public function Region(){
        return $this->belongsTo('App\Regiao', 'regiao_id');
    }
}
