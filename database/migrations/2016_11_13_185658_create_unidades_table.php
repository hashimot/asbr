<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidades', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('regiao_id')->unsigned();
            $table->string('name',255);
            $table->foreign('regiao_id')->references('id')->on('regioes')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->timestamps();
        });
        /*
         * UNIDADE
         * Cada região possui um set diferente de unidades e isso deve ficar claro para o usuário no formulário:
            Sul - Porto Alegre, Curitiba
            Sudeste - São Paulo, Rio de Janeiro, Belo Horizonte
            Centro-Oeste - Brasília
            Nordeste - Salvador, Recife
            Norte - Não possui disponibilidade
         */
        $data_to_insert = array(
            array(
                'regiao_id' => 1,
                'name' => 'Porto Alegre'
            ), array(
                'regiao_id' => 1,
                'name' => 'Curitiba'
            ), array(
                'regiao_id' => 2,
                'name' => 'São Paulo'
            ), array(
                'regiao_id' => 2,
                'name' => 'Rio de Janeiro'
            ), array(
                'regiao_id' => 2,
                'name' => 'Belo Horizonte'
            ), array(
                'regiao_id' => 3,
                'name' => 'Brasília'
            ), array(
                'regiao_id' => 4,
                'name' => 'Salvador'
            ), array(
                'regiao_id' => 4,
                'name' => 'Recife'
            ), array(
                'regiao_id' => 5,
                'name' => 'INDISPONÍVEL'
            )
        );
        DB::table('unidades')->insert($data_to_insert);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidades');
    }
}
