<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;



class CreateRegioesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regioes', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 255);
            $table->integer('pontos');
            $table->timestamps();
        });
        /*
         * Região
        Sul: -2 pontos
        Sudeste: -1 ponto, exceto quando unidade = São Paulo (que não modifica)
        Centro-Oeste: -3 pontos
        Nordeste: -4 pontos
        Norte: -5 pontos
        */
        // Insert some stuff
        $data_to_insert = array(
            array(
                'name' => 'Sul',
                'pontos' => -2
            ),
            array(
                'name' => 'Sudeste',
                'pontos' => -1
            ),
            array(
                'name' => 'Centro-Oeste',
                'pontos' => -3
            ),
            array(
                'name' => 'Nordeste',
                'pontos' => -4
            ),
            array(
                'name' => 'Norte',
                'pontos' => -5
            )
        );
        DB::table('regioes')->insert($data_to_insert);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regioes');
    }
}
